
import {Accordion, Button, TextField} from '@mui/material';
import SearchAppBar from './components/AppBar';
import { ThemeProvider } from '@mui/material/styles';
import AppBarTheme from './themes/AppBarTheme'
import CustomizedAccordions from './components/Accordion';
import CollapsibleTable from './components/TableCharts';
import Table from '@mui/material/Table';
import React from 'react';

const App = () => {
  const [search, setSearch] = React.useState('');

  const handleSearch = (event) => {
    setSearch(event.target.value);
  };

  const data = {
    nodes: list.filter((item) =>
      item.name.includes(search)
    ),
  };

  return (
    <>
      <label htmlFor="search">
        Search by Task:
        <input id="search" type="text" onChange={handleSearch} />
      </label>

      <Table data={data}>
        ...
      </Table>
    </>
  );
};


// function App() {
//   return (
//   <>
//     <ThemeProvider theme={AppBarTheme}>
//       <SearchAppBar/>
//     <CollapsibleTable/>
//     </ThemeProvider>
//   </>
//   );
 
// }

export default App;
